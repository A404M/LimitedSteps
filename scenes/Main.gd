extends Node2D

onready var map = $MapHolder.get_child(0)

func changeMap(newMap:TileMap):
	$MapHolder.remove_child($MapHolder.get_child(0))
	$MapHolder.add_child(newMap)
	newMap.owner = $MapHolder
	map = newMap
	startGame()

func _ready():
	startGame()

func _unhandled_input(event):
	if event.is_action_pressed("full_screen"):
		OS.window_fullscreen = not OS.window_fullscreen
	elif event.is_action_pressed("start"):
		startGame()

func gameOver():
	$Player.showGameOver()

func wonGame():
	var loaded = load("res://scenes/map/maps/M"+String(int(map.name.substr(1))+1)+".tscn")
	if loaded == null:
		$Player.showYouWon()
	else:
		changeMap(loaded.instance())
	#$Player.showYouWon()

func startGame():
	$Player.hideEndText()
	$Player.map = map
	$Player.distination = map.getSpawnPosition()
	$Player.origin = $Player.distination
	$Player.global_position = $Player.origin
	$Player.stepsRemains = map.steps
	$Player.healthRemains = map.health


func _on_Player_dead():
	gameOver()


func _on_Player_outOfSteps():
	gameOver()


func _on_Player_enteredDoor(pos):
	wonGame()
