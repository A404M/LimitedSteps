extends KinematicBody2D

signal dead
signal outOfSteps
signal enteredDoor(pos)

export var hStepSize := 16
export var vStepSize := 100
export var stepsRemains := 100 setget setStepsRemains
export var healthRemains := 2 setget setHealthRemains

export var jump_time_to_peak := 0.4
export var jump_time_to_descent := 0.3

onready var jump_hight:float = vStepSize
onready var jump_velocity:float = ((2.0*jump_hight)/jump_time_to_peak)*-1.0
onready var slide_velocity:float = (((2.0*jump_hight)/jump_time_to_peak)*-1.0)/2.0
onready var jump_gravity:float = ((-2.0*jump_hight)/(jump_time_to_peak * jump_time_to_peak))*-1.0
onready var fall_gravity:float = ((-2.0*jump_hight)/(jump_time_to_descent * jump_time_to_descent))*-1.0

var velocity := Vector2.ZERO
var distination := Vector2.ZERO
var origin := Vector2.ZERO
var t := 0.0
var gotDamaged := false

var map:TileMap = null


func setStepsRemains(val):
	stepsRemains = val
	updateStepsRemains()

func setHealthRemains(val):
	healthRemains = val
	updateHealthReamains()

func updateStepsRemains():
	$Camera2D/Steps.text = "Steps: %d"%stepsRemains

func updateHealthReamains():
	$Camera2D/Health.text = "Health: %d"%healthRemains

func _ready():
	origin = global_position
	distination = global_position
	updateHealthReamains()
	updateStepsRemains()

func _unhandled_input(event):
	if event is InputEventScreenTouch:
		if event.pressed:
			swipe_start = event.get_position()
		else:
			_calculate_swipe(event.get_position())

var swipe_start = null
var minimum_drag = 50

func _calculate_swipe(swipe_end):
	if swipe_start == null: 
		return
	var swipe = swipe_end - swipe_start
	if swipe.y < -minimum_drag:
		Input.action_press("jump")
	elif abs(swipe.x) > minimum_drag:
		if swipe.x > 0:
			Input.action_press("right")
		else:
			Input.action_press("left")


func _process(delta):
	$Camera2D/FPS.text = "FPS: %d"%Engine.get_frames_per_second()

func _physics_process(delta):
	if $Camera2D/EndText.visible:
		return
	velocity.y += getGravity()*delta
	var direction := getDirection()
	
	if direction == Vector2.UP:
		if is_on_floor():
			jump()
		direction = Vector2.ZERO
	
	if direction != Vector2.ZERO:
		stepsRemains -= 1
		updateStepsRemains()
		if stepsRemains == 0:
			emit_signal("outOfSteps")
	
	
	velocity = move_and_slide(velocity,Vector2.UP)
	
	if direction.x != 0:
		origin = global_position
		distination.x += direction.x*hStepSize
		t = 0.0
	if t < 1.0:
		t += delta*4
	else:
		t = 1.0
	
	var gd := false
	for i in get_slide_count():
		var collision = get_slide_collision(i)
		
		if collision.collider.name == map.name:
			#print(map.getTileNameAt(collision.position))
			var cellName = map.getTileNameAt(collision.position)
			if "sharp" in cellName:
				gd = true
			elif "door" in cellName:
				emit_signal("enteredDoor",collision.position)
	
	if gd != gotDamaged:
		gotDamaged = gd
		if gotDamaged:
			healthRemains -= 1
			updateHealthReamains()
			if healthRemains == 0:
				emit_signal("dead")
	
	if is_on_wall():
		t = 1.0
		global_position.x += 1 if distination.x < global_position.x else -1
		distination = global_position
	else:
		global_position.x = lerp(origin.x,distination.x,t)
	
	
	var moveDir := getMovingDirection()
	
	if moveDir == Vector2.UP+Vector2.RIGHT:
		$AnimationPlayer.play("top_right")
	elif moveDir == Vector2.UP+Vector2.LEFT:
		$AnimationPlayer.play("top_left")
	elif moveDir == Vector2.UP:
		$AnimationPlayer.play("top")
	elif moveDir == Vector2.RIGHT:
		$AnimationPlayer.play("right")
	elif moveDir == Vector2.LEFT:
		$AnimationPlayer.play("left")
	else:
		$AnimationPlayer.play("idle")


func getMovingDirection()->Vector2:
	var dir := Vector2.ZERO
	if not is_on_floor():
		dir += Vector2.UP
	
	if distination.x > global_position.x:
		dir += Vector2.RIGHT
	elif distination.x < global_position.x:
		dir += Vector2.LEFT
	
	return dir

func getGravity() -> float:
	return jump_gravity if velocity.y < 0.0 else fall_gravity

func jump():
	velocity = getJumpVelocity(Vector2.UP)

func getJumpVelocity(dir:Vector2)->Vector2:
	var vel = Vector2(
			slide_velocity,
			jump_velocity
		)
	vel *= dir
	return -vel

func getDirection()->Vector2:
	if Input.is_action_just_pressed("jump"):
		return Vector2.UP
	elif Input.is_action_just_pressed("left"):
		return Vector2.LEFT
	elif Input.is_action_just_pressed("right"):
		return Vector2.RIGHT
	else:
		return Vector2.ZERO


func showGameOver():
	$Character.hide()
	$Camera2D/EndText.text = "Game Over"
	$Camera2D/EndText.show()


func showYouWon():
	$Character.hide()
	$Camera2D/EndText.text = "You Won"
	$Camera2D/EndText.show()


func hideEndText():
	$Camera2D/EndText.hide()
	$Character.show()
