extends TileMap

export var health := 2
export var steps := 100

func getTileNameAt(pos:Vector2):
	var mpos = world_to_map(pos)
	var id = get_cellv(mpos)
	if id == -1:
		return ""
	var cellName = tile_set.tile_get_name(id)
	return cellName


func getSpawnPosition()->Vector2:
	return $SpawnPoint.global_position
